<?php

namespace User\Controller;

use DoctrineModule\Paginator\Adapter\Selectable;
use Zend\Paginator\Paginator;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Form\Annotation\AnnotationBuilder;
use User\Entity\User;

/**
 * Web form for User Entity
 * Class UserController
 * @package User\Controller
 */
class UserController extends AbstractActionController
{

    /**
     * List All users
     * @return array
     */
    public function indexAction()
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        $adapter = new Selectable($em->getRepository('User\Entity\User'));

        $paginator = new Paginator($adapter);
        $page = 1;

        if ($this->params()->fromQuery('page')) {
            $page = $this->params()->fromQuery('page');
        }

        $paginator->setCurrentPageNumber($page)->setItemCountPerPage(2);

        return ['paginator' => $paginator];
    }

    /**
     * Creates a user
     * @return array|\Zend\Http\Response
     */
    public function addAction()
    {
        $user = new User();
        $builder = new AnnotationBuilder();
        $form = $builder->createForm($user);

        if ($this->getRequest()->isPost()) {
            $form->bind($user);
            $form->setData($this->getRequest()->getPost());

            if ($form->isValid()) {
                $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
                $em->persist($user);
                $em->flush();

                $this->flashMessenger()->addMessage('Successfully saved');
                return $this->redirect()->toRoute('user');
            }
        }

        return ['form' => $form];
    }

    /**
     * Edits a user
     * @return array|\Zend\Http\Response
     */
    public function editAction()
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $user = $em->find('User\Entity\User', $this->params()->fromRoute('id'));

        $builder = new AnnotationBuilder();
        $form = $builder->createForm($user);
        $form->bind($user);
        $form->get('submit')->setAttribute('value', 'Edit');

        if ($this->getRequest()->isPost()) {
            $form->setData($this->getRequest()->getPost());

            if ($form->isValid()) {
                $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
                $em->flush();

                $this->flashMessenger()->addMessage('Successfully edited');
                return $this->redirect()->toRoute('user', ['action' => 'index']);
            }
        }

        return [
            'form' => $form,
            'id' => $this->params()->fromRoute('id'),
        ];
    }

    /**
     * Deletes a user
     * @return array|\Zend\Http\Response
     */
    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id');
        if (!$id) {
            return $this->redirect()->toRoute('user', ['action' => 'index']);
        }

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $user = $em->find('User\Entity\User', $id);

        if ($this->getRequest()->isPost()) {
            if ($this->getRequest()->getPost('del') == 'Yes') {
                $em->remove($user);
                $em->flush();
                $this->flashMessenger()->addMessage('Successfully deleted');
            }

            return $this->redirect()->toRoute('user', ['action' => 'index']);
        }

        return array(
            'id' => $id,
            'user' => $user
        );
    }


}


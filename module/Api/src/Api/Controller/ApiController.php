<?php
namespace Api\Controller;

use Zend\Form\Annotation\AnnotationBuilder;
use Zend\Mvc\Controller\AbstractRestfulController;
use User\Entity\User;
use Zend\View\Model\JsonModel;

/**
 * API for User Entity
 * Class ApiController
 * @package Api\Controller
 */
class ApiController extends AbstractRestfulController
{
    /**
     * List all users
     * @return JsonModel json
     */
    public function getList()
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $users = $em->getRepository('User\Entity\User')->findAll();

        $data = array();
        foreach ($users as $result) {
            $data[] = $result;
        }

        return new JsonModel(array(
            'data' => $data,
        ));
    }

    /**
     * List user based on id
     * @param mixed $id
     * @return JsonModel json
     */
    public function get($id)
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $user = $em->find('User\Entity\User', $id);

        return new JsonModel(array(
            'data' => $user,
        ));
    }

    /**
     * Creates a user
     * @param mixed $data
     * @return JsonModel json
     */
    public function create($data)
    {
        $user = new User();
        $builder = new AnnotationBuilder();
        $form = $builder->createForm($user);
        $form->bind($user);
        $form->setData($data);

        $message = '';
        if ($form->isValid()) {
            $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $em->persist($user);
            $em->flush();

            $message = 'saved';
        } else {
            $message = $form->getMessages();
        }

        return new JsonModel(array(
            'data' => $data,
            'message' => $message,
        ));
    }

    /**
     * Updates a user
     * @param mixed $id
     * @param mixed $data
     * @return JsonModel json
     */
    public function update($id, $data)
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $user = $em->find('User\Entity\User', $id);
        $builder = new AnnotationBuilder();
        $form = $builder->createForm($user);
        $form->bind($user);
        $form->setData($data);

        $message = '';
        if ($form->isValid()) {
            $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
            $em->flush();

            $message = 'updated';
        } else {
            $message = $form->getMessages();
        }

        return new JsonModel(array(
            'data' => $data,
            'message' => $message,
        ));
    }

    /**
     * Deletes a user based on id
     * @param mixed $id
     * @return JsonModel json
     */
    public function delete($id)
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $user = $em->find('User\Entity\User', $id);
        $em->remove($user);
        $em->flush();

        return new JsonModel(array(
            'data' => 'deleted',
        ));
    }
}